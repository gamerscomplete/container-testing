package main

import (
	"fmt"
	"os"
)

func main() {
	if os.Args[0] == "unc-fork" {
		if err := fork(); err != nil {
			fmt.Println("Fork error:", err)
			os.Exit(1)
		}
		os.Exit(0)
	}

	args := os.Args[1:]
	if len(args) == 0 {
		args = []string{os.Getenv("SHELL")}
	}

	c := &Container{
		Args: args,
		Uid:  os.Getuid(),
		Gid:  os.Getgid(),
	}
	if err := c.Start(); err != nil {
		fmt.Println("Container start failed: ", err)
		os.Exit(1)
	}
}
