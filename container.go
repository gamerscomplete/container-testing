package main

import (
	"fmt"
	"os"
	"os/exec"
	"syscall"
	"path/filepath"
)

/*const (
	_IMAGE_NAME = "alpine"
	_IMAGE_NAME = "debian"
)*/

type Container struct {
	Args []string
	Uid  int
	Gid  int
}

type Mount struct {
	Source string
	Target string
	Fs     string
	Flags  int
	Data   string
}

type Cfg struct {
	Path     string
	Args     []string
	Hostname string
	Mounts   []Mount
	Rootfs   string
}

var defaultMountFlags = syscall.MS_NOEXEC | syscall.MS_NOSUID | syscall.MS_NODEV

var defaultCfg = Cfg{
	Hostname: "unc",
	Mounts: []Mount{
		{
			Source: "proc",
			Target: "/proc",
			Fs:     "proc",
			Flags:  defaultMountFlags,
		},
		{
			Source: "tmpfs",
			Target: "/dev",
			Fs:     "tmpfs",
			Flags:  syscall.MS_NOSUID | syscall.MS_STRICTATIME,
			Data:   "mode=755",
		},
	},
	Rootfs: "/home/chris/project/alpine",
}

func (c *Container) Start() error {
	cmd := &exec.Cmd{
		Path: os.Args[0],
		Args: append([]string{"unc-fork"}, c.Args...),
	}
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.SysProcAttr = &syscall.SysProcAttr{}
	cmd.SysProcAttr.Cloneflags = syscall.CLONE_NEWUSER | syscall.CLONE_NEWPID | syscall.CLONE_NEWUTS | syscall.CLONE_NEWNS

	cmd.SysProcAttr.UidMappings = []syscall.SysProcIDMap{
		{
			ContainerID: 0,
			HostID:      c.Uid,
			Size:        1,
		},
	}

	cmd.SysProcAttr.GidMappings = []syscall.SysProcIDMap{
		{
			ContainerID: 0,
			HostID:      c.Gid,
			Size:        1,
		},
	}
		/*cmd.SysProcAttr.Setctty = true
		cmd.SysProcAttr.Setsid = true*/
	fmt.Print("Running command \"", c.Args, "\" as ", c.Uid, ":", c.Gid, "\n")
	return cmd.Run()
}

func pivotRoot(root string) error {
	if err := syscall.Mount(root, root, "bind", syscall.MS_BIND|syscall.MS_REC, ""); err != nil {
		return fmt.Errorf("Mount rootfs to itself error: %v", err)
	}
	pivotDir := filepath.Join(root, ".pivot_root")
	if err := os.Mkdir(pivotDir, 0777); err != nil {
		return fmt.Errorf("pivot_root %v", err)
	}
	fmt.Println("Pivot root dir:", pivotDir)
	fmt.Println("Pivot root to", root)

	if err := syscall.PivotRoot(root, pivotDir); err != nil {
		return fmt.Errorf("pivot_root %v", err)
	}

	if err := syscall.Chdir("/"); err != nil {
		return fmt.Errorf("chdir / %v", err)
	}
	pivotDir = filepath.Join("/", ".pivot_root")
	if err := syscall.Unmount(pivotDir, syscall.MNT_DETACH); err != nil {
		return fmt.Errorf("unmount pivot_root dir %v", err)
	}

	return os.Remove(pivotDir)
}

func mount(cfg Cfg) error {
	for _, m := range cfg.Mounts {
		target := filepath.Join(cfg.Rootfs, m.Target)
		fmt.Println("Mount", m.Source, "to", target)
		if err := syscall.Mount(m.Source, target, m.Fs, uintptr(m.Flags), m.Data); err != nil {
			return fmt.Errorf("Failed to mount %s to %s: %v", m.Source, target, err)
		}
	}
	return nil
}

func setup(cfg Cfg) error {
	fmt.Println("Mounting cfg")
	if err := mount(cfg); err != nil {
		return err
	}
	fmt.Println("Pivoting root")
	if err := pivotRoot(cfg.Rootfs); err != nil {
		return fmt.Errorf("Pivot root error: %v", err)
	}
	fmt.Println("Setting hostname")
	if err := syscall.Sethostname([]byte(cfg.Hostname)); err != nil {
		return fmt.Errorf("Sethostname: %v", err)
	}
	fmt.Println("Setup all gud")
	return nil
}

func execProc(cfg Cfg) error {
	fmt.Println("Execute", append([]string{cfg.Path}, cfg.Args[1:]...))
	return syscall.Exec(cfg.Path, cfg.Args, os.Environ())
}

func fillCfg() error {
	name, err := exec.LookPath(os.Args[2])
	if err != nil {
		return fmt.Errorf("LookPath: %v", err)
	}
	defaultCfg.Path = name
	defaultCfg.Args = os.Args[2:]
	wd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("Error get working directory: %v", err)
	}
	defaultCfg.Rootfs = wd + "/images/" + os.Args[1]
	return nil
}

func fork() error {
	fmt.Println("Start fork")
	if err := fillCfg(); err != nil {
		return err
	}
	if err := setup(defaultCfg); err != nil {
		return err
	}
	return execProc(defaultCfg)
}
